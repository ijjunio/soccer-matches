from django.urls import path
from .views import ItemList, ItemDetail, LocationList, LocationDetail

urlpatterns = [
  path('items/', ItemList.as_view(), name='item_list'),
  path('items/<int:pk>/', ItemDetail.as_view(), name='item_detail'),
  path('locations/', LocationList.as_view(), name='location_list'),
  path('locations/<int:pk>/', LocationDetail.as_view(), name='location_detail'),
]