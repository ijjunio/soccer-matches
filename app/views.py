from django.shortcuts import render, redirect
from .models import Player

#@login_required(login_url='/login/')
def home(request):
  players = Player.objects.all()
  return render(request, 'app/index.html', {'players': players})

def save(request):
  nome = request.POST.get('nome')
  preco = request.POST.get('preco')
  Player.objects.create(name=nome, initial_price=preco)
  players = Player.objects.all()
  return render(request, 'app/index.html', {'players': players})
  
def edit(request, id):
  player = Player.objects.get(id=id)
  return render(request, 'app/update.html', {'player': player})

def update(request, id):
  nome = request.POST.get('nome')
  preco = request.POST.get('preco')
  player = Player.objects.get(id=id)
  player.name = nome
  player.initial_price = preco
  player.save()
  return redirect('home')

def delete(request, id):
  player = Player.objects.get(id=id)
  player.delete()
  return redirect('home')