from .views import home, save, update, edit, delete
from django.urls import path

urlpatterns = [
    path('', home, name='home'),
    path('save/', save, name='save'),
    path('edit/<int:id>', edit, name='edit'),
    path('update/<int:id>', update, name='update'),
    path('delete/<int:id>', delete, name='delete'),
]
