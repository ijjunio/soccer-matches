# Use a imagem base do Python
FROM python:3.10.2-slim

# Crie um usuário
RUN useradd -ms /bin/bash python

# Instale o Poetry
RUN pip install poetry

# Mude para o usuário python
USER python

# Defina o diretório de trabalho
WORKDIR /home/python/app

# Copie pyproject.toml e poetry.lock para a imagem
#COPY --chown=python:python pyproject.toml poetry.lock* ./

# Instale as dependências
#RUN poetry install --no-interaction --no-ansi

# Mantenha o container rodando
CMD ["tail", "-f", "/dev/null"]